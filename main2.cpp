#include <bits/stdc++.h>

using namespace std;

string toString(int p)
{
	ostringstream ss;
	ss<<p;
	return ss.str();
}

void splitString(string message, string delimiter, string result[], int n) {
    int i = 0, pos = 0, length = 0, temp;
    temp = message.find ( delimiter.c_str ( ), pos );
    while ( temp != -1 )
    {
        length = temp - pos;
        result[i] = message.substr ( pos, length );
        pos = temp + delimiter.size ( );
        temp = message.find ( delimiter.c_str ( ), pos );
        i++;
    }
    result[i] = message.substr ( pos );
    i++;
}

int main(int argc, char const *argv[])
{
	string filename = argv[1];
	string outputFile = filename+".out";
	ofstream fout;
	ifstream fin;
	string line;
	fout.open(outputFile.c_str());
	string planFile = filename+".plan";
	fin.open(planFile.c_str());
	bool solved = false;
	vector<string> lines,output;
	while (getline(fin, line)) {
		lines.push_back(line);
		if(line == "Solution found!")
			solved = true;
	}
	if(solved){
		int l = lines.size();
		string num = lines[l-16];
		num = num.substr(11,num.size()-11);
		int n = atoi(num.c_str());
		if(n != 0){
			string str[8];
			string s,currentId,prevId;
			char currentMove,prevMove;
			int count = 1;
			s = lines[l-17-n];
			splitString(s," ",str,8);
			prevId = str[1].substr(3,str[1].size()-3);
			prevMove = str[0].at(4) - 32;
			for (int i = 1; i < n; ++i)
			{
				s = lines[l-17-n+i];
				splitString(s," ",str,8);
				currentId = str[1].substr(3,str[1].size()-3);
				currentMove = str[0].at(4) - 32;
				if ((currentId == prevId) && (currentMove == prevMove))
					count++;
				else{
					s = prevId + " " + prevMove + " " + toString(count);
					output.push_back(s);
					count = 1;
					prevMove = currentMove;
					prevId = currentId;
				}
			}
			s = prevId + " " + prevMove + " " + toString(count);
			output.push_back(s);
			fout<<output.size()<<endl;
			for (int i = 0; i < output.size(); ++i)
			{
				fout<<output[i]<<endl;
			}
		}
		else
		{
			fout<<0<<endl;
		}
	}
	else
	{
		fout<<-1<<endl;
	}
	fin.close();
	fout.close();
	return 0;
}