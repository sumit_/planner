#include <bits/stdc++.h>

using namespace std;

string toString(int p)
{
	ostringstream ss;
	ss<<p;
	return ss.str();
}

struct car
{
	int id;
	int length;
	int x_left;
	int y_top;
	char dir;
	string name;
};

int main(int argc, char const *argv[])
{
	string filename = argv[1];
	string problemFile = filename+".txt";
	ifstream fin;
	fin.open(problemFile.c_str());
	int M,N;
	int goalx,goaly;
	fin>>M>>N;
	int numCars;
	fin>>numCars;
	car *A = new car[numCars];
	char flag;
	int id,x_left,y_top,len;
	for (int i = 0; i < numCars; ++i)
	{
		fin>>A[i].id>>A[i].length>>A[i].x_left>>A[i].y_top>>A[i].dir;
		A[i].name = "car" + toString(A[i].id);
	}
	fin>>goalx>>goaly;
	fin.close();
	int **cells = new int*[M];
	for (int i = 0; i < M; ++i)
	{
		cells[i] = new int[N];
		for (int j = 0;j < N; ++j)
			cells[i][j] = 0;
	}
	for (int i = 0; i < numCars; ++i)
	{
		if(A[i].dir == 'H'){
			int s = A[i].x_left;
			int y = A[i].y_top;
			int l = A[i].length;
			for (int j = 0; j < l; ++j){
				cells[y-1][s+j-1] = 1;
			}
		}
		else{
			int s = A[i].x_left;
			int y = A[i].y_top;
			int l = A[i].length;
			for (int j = 0; j < l; ++j){
				cells[y+j-1][s-1] = 1;
			}
		}
	}
	cout<<"\n";
	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cout<<cells[i][j]<<" ";
		}
		cout<<"\n";
	}
	string domainFileName = filename+"-domain.pddl";
	ofstream fout;
	fout.open(domainFileName.c_str());
	fout<<"(define (domain congested-parking-lot)\n\t(:predicates \t(car ?c) (horizontal ?c) (vertical ?c)"
		<<"\n\t\t\t\t\t(empty ?x ?y) (succ ?a ?b) (pred ?a ?b)"
		<<"\n\t\t\t\t\t(xleft ?c ?x) (xright ?c ?x) (ytop ?c ?y) (ydown ?c ?y))\n\n";
	
	fout<<"\t(:action moveleft\n\t\t:parameters (?c ?x1 ?y1 ?x2 ?x3 ?x4)\n\t\t:precondition (and (car ?c) (horizontal ?c) "
		<<"(xleft ?c ?x1) (ytop ?c ?y1) (xright ?c ?x3) "
		<<"(empty ?x2 ?y1) (pred ?x2 ?x1) (pred ?x4 ?x3))\n\t\t:effect (and (xleft ?c ?x2) "
		<<"(not (xleft ?c ?x1)) (xright ?c ?x4) (not (xright ?c ?x3)) (empty ?x3 ?y1) (not (empty ?x2 ?y1)))\n\t)\n\n";

	fout<<"\t(:action moveright\n\t\t:parameters (?c ?x1 ?y1 ?x2 ?x3 ?x4)\n\t\t:precondition (and (car ?c) (horizontal ?c) "
		<<"(xleft ?c ?x1) (ytop ?c ?y1) (xright ?c ?x3) "
		<<"(empty ?x4 ?y1) (succ ?x2 ?x1) (succ ?x4 ?x3))\n\t\t:effect (and (xleft ?c ?x2) "
		<<"(not (xleft ?c ?x1)) (xright ?c ?x4) (not (xright ?c ?x3)) (empty ?x1 ?y1) (not (empty ?x4 ?y1)))\n\t)\n\n";

	fout<<"\t(:action moveup\n\t\t:parameters (?c ?x1 ?y1 ?y2 ?y3 ?y4)\n\t\t:precondition (and (car ?c) (vertical ?c) "
		<<"(xleft ?c ?x1) (ytop ?c ?y1) (ydown ?c ?y3) "
		<<"(empty ?x1 ?y2) (pred ?y2 ?y1) (pred ?y4 ?y3))\n\t\t:effect (and (ytop ?c ?y2) "
		<<"(not (ytop ?c ?y1)) (ydown ?c ?y4) (not (ydown ?c ?y3)) (empty ?x1 ?y3) (not (empty ?x1 ?y2)))\n\t)\n\n";

	fout<<"\t(:action movedown\n\t\t:parameters (?c ?x1 ?y1 ?y2 ?y3 ?y4)\n\t\t:precondition (and (car ?c) (vertical ?c) "
		<<"(xleft ?c ?x1) (ytop ?c ?y1) (ydown ?c ?y3) "
		<<"(empty ?x1 ?y4) (succ ?y2 ?y1) (succ ?y4 ?y3))\n\t\t:effect (and (ytop ?c ?y2) "
		<<"(not (ytop ?c ?y1)) (ydown ?c ?y4) (not (ydown ?c ?y3)) (empty ?x1 ?y1) (not (empty ?x1 ?y4)))\n\t)\n\n";

	fout<<"\n)\n";
	fout.close();
	string problemFileName = filename+".pddl";
	fout.open(problemFileName.c_str());
	fout<<"(define (problem parking-lot)\n\t(:domain congested-parking-lot)\n\t(:objects ";
	for (int i = 0; i < numCars; ++i)
		fout<<A[i].name<<" ";
	int m = max(M,N);
	for (int i = 1; i < m; ++i)
		fout<<i<<" ";
	fout<<m<<")\n\n";
	fout<<"\t(:init\n\t\t";
	for (int i = 0; i < numCars; ++i)
		fout<<"(car "<<A[i].name<<")\n\t\t";
	for (int i = 0; i < numCars; ++i){
		if(A[i].dir == 'H'){
			fout<<"(horizontal "<<A[i].name<<")\n\t\t";
			fout<<"(xleft "<<A[i].name<<" "<<A[i].x_left<<")\n\t\t";
			fout<<"(xright "<<A[i].name<<" "<<A[i].x_left+A[i].length-1<<")\n\t\t";
			fout<<"(ytop "<<A[i].name<<" "<<A[i].y_top<<")\n\t\t";
			fout<<"(ydown "<<A[i].name<<" "<<A[i].y_top<<")\n\t\t";
		}
		else{
			fout<<"(vertical "<<A[i].name<<")\n\t\t";
			fout<<"(xleft "<<A[i].name<<" "<<A[i].x_left<<")\n\t\t";
			fout<<"(xright "<<A[i].name<<" "<<A[i].x_left<<")\n\t\t";
			fout<<"(ytop "<<A[i].name<<" "<<A[i].y_top<<")\n\t\t";
			fout<<"(ydown "<<A[i].name<<" "<<A[i].y_top+A[i].length-1<<")\n\t\t";
		}
	}
	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			if(cells[i][j] == 0){
				fout<<"(empty "<<j+1<<" "<<i+1<<")\n\t\t";
			}
		}
	}
	for (int i = 1; i < m; ++i)
	{
		fout<<"(succ "<<i+1<<" "<<i<<")\n\t\t";
		fout<<"(pred "<<i<<" "<<i+1<<")\n\t\t";
	}
	if(A[0].dir == 'H'){
		fout<<"\n\t)\n\n\t(:goal (and ";
		fout<<"(xleft "<<A[0].name<<" "<<goalx<<") ";
        fout<<"(ytop "<<A[0].name<<" "<<goaly<<") ";
		fout<<"(= "<<1<<" "<<goalx<<") ";
		fout<<") )\n\n)\n";
	}
	else{
		fout<<"\n\t)\n\n\t(:goal (and ";
        fout<<"(xleft "<<A[0].name<<" "<<goalx<<") ";
		fout<<"(ytop "<<A[0].name<<" "<<goaly<<") ";
        fout<<"(= "<<1<<" "<<goaly<<") ";
		fout<<") )\n\n)\n";
	}
	
	fout.close();
	return 0;
}